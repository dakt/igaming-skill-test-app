export default {
    
    setItem(key, data) {
        window.localStorage.setItem(key, data);
    },

    getItem(key, data) {
        return window.localStorage.getItem(key);
    },

    getAll() {
        return window.localStorage
    },
    
};