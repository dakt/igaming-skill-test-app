import React, { Component } from 'react';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk'
import { Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import SessionManager from './helpers/sessionManager';
import rootReducer from './rootReducer';
import { Header, Footer } from './pages/segments';
import LimitsForm from './pages/limits/form';
import { NotImplemented, NoMatch } from './pages/misc';
import initialState from './limits.json';

import './app.css';


function initializeState() {
  const json = require('./limits.json');

  let state = {};
  const locStorage = SessionManager.getAll();

  state.formLimits = {};
  state.formLimits.bet_limit = localStorage.bet_limit || json.bet.amount;
  state.formLimits.bet_limit_period = localStorage.bet_limit_period || json.bet.period;
  state.formLimits.deposit_limit = localStorage.deposit_limit || json.deposit.amount;
  state.formLimits.deposit_limit_period = localStorage.deposit_limit_period || json.deposit.period;
  state.formLimits.loss_limit = localStorage.loss_limit || json.loss.amount;
  state.formLimits.loss_limit_period = localStorage.loss_limit_period || json.loss.period;

  return state;
};

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  initializeState(),
  composeEnhancers(
    applyMiddleware(
      ReduxThunk
    )
  ),
);

const history = createBrowserHistory({});

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router history={history}>
          <div>
            <Header title="ExperienceHub" subtitle="Platform" />
            <section className="section">
                <Switch>
                  <Route path="/account" component={NotImplemented} />
                  <Route path="/casher" component={NotImplemented} />
                  <Route path="/balances" component={NotImplemented} />
                  <Route path="/bonuses" render={NotImplemented} />
                  <Route path="/transaction-history" component={NotImplemented} />
                  <Route path="/limits" component={LimitsForm} />
                  <Route component={NoMatch} />
                </Switch>
            </section>
            <Footer />
          </div>
        </Router>
      </Provider>
    );
  }
}

export { history }

export default App;
