import { combineReducers } from 'redux';

import formLimits from './pages/limits/redux'


export default combineReducers({
    formLimits,
})