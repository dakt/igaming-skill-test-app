import React from 'react';

export default () => (
    <div className="notification is-warning">
        <div className="title">
            <i className="fa fa-times" aria-hidden="true"></i> Not implemented
        </div>
    </div>
);