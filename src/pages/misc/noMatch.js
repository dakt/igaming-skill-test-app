import React from 'react';

export default () => (
    <div className="notification is-warning">
        <div className="title">
            <i className="fa fa-times" aria-hidden="true"></i> Oops!
        </div>
        <div className="subtitle">
            We can't seem to find the page you're looking for.
        </div>
        
    </div>
);