import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from '../../components/controls';

import { Input, Select } from '../../components/form';
import { Button } from '../../components/controls';
import * as Actions  from './redux';

const PERIODS = ["day", "week", "month"];

class StepOne extends Component {
    render() {
        return (
            <div>
                <div className="columns">
                    <div className="column is-12">
                        <p className="title">ACCOUNT LIMITS</p>
                    </div>
                </div>
                <div className="columns">
                    <div className="column is-12"><label>BET LIMNIT(€)</label></div>
                </div>
                <div className="columns">
                    <div className="column is-6">
                        <Input
                            value={this.props.fields.bet_limit}
                            onChange={(e) => this.props.handleFieldChange("bet_limit", e.target.value)}
                            type="number"
                        />
                    </div>
                    <div className="column is-6">
                        <Select
                            value={this.props.fields.bet_limit_period}
                            values={PERIODS}
                            onChange={(value) => this.props.handleFieldChange("bet_limit_period", value)}
                        />
                    </div>
                </div>
                <div className="columns">
                    <div className="column is-12"><label>DEPOSIT LIMIT(€)</label></div>
                </div>
                <div className="columns">
                    <div className="column is-6">
                        <Input
                            value={this.props.fields.deposit_limit}
                            onChange={(e) => this.props.handleFieldChange("deposit_limit", e.target.value)}
                            type="number"
                        />
                    </div>
                    <div className="column is-6">
                        <Select 
                            value={this.props.fields.deposit_limit_period}
                            values={PERIODS}
                            onChange={(value) => this.props.handleFieldChange("deposit_limit_period", value)}
                        />
                    </div>
                </div>
                <div className="columns">
                    <div className="column is-12"><label>LOSS LIMIT(€)</label></div>
                </div>
                <div className="columns">
                    <div className="column is-6">
                        <Input
                            value={this.props.fields.loss_limit}
                            onChange={(e) => this.props.handleFieldChange("loss_limit", e.target.value)}
                            type="number"
                        />
                    </div>
                    <div className="column is-6">
                        <Select 
                            value={this.props.fields.loss_limit_period}
                            values={PERIODS}
                            onChange={(value) => this.props.handleFieldChange("loss_limit_period", value)}
                        />
                    </div>
                </div>
                <div className="columns">
                    <div className="column is-12">
                        {this.props.fields.limitsError && (
                            <article className="tile notification is-danger">{this.props.fields.limitsError}</article>
                        )}
                    </div>
                </div>
                <div className="columns">
                    <div className="column is-12">
                        <Link
                            icon="arrow-right"
                            as="button"
                            type="link"
                            onClick={() => this.props.setAndValidateLimits()}
                        >SET LIMIT</Link>
                    </div>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state, action) => ({
    fields: state.formLimits,
});

const mapStateToDispatch = (dispatch) => ({
    handleFieldChange: (field, value) => dispatch(Actions.PersistantFieldChange(field, value)),
    setAndValidateLimits: () => dispatch(Actions.SetAndValidateLimits()),
});

export default connect(
    mapStateToProps,
    mapStateToDispatch
)(StepOne);