import React, { Component } from 'react';


export default class StepThree extends Component {
    render() {
        return (
            <div className="columns">
                <article className="tile notification is-success">
                    <div className="column is-2">
                        <i className="fa fa-check-circle fa-5x" aria-hidden="true"></i>
                    </div>
                    <div className="column is-10">
                        <p className="title">SUCCESS!</p>
                        <p className="subititle">New limits are now set.</p>
                    </div>
                </article>
            </div>
        );
    }
}