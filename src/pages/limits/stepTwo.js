import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import { connect } from 'react-redux';

import { Input } from '../../components/form';
import { Link } from '../../components/controls';
import * as Actions from './redux';

class StepTwo extends Component {

    componentDidMount() {
        const pwdDOMnode = findDOMNode(this.pwdInput);
        pwdDOMnode.focus();
    }

    handleKeyUp(e) {
        e.keyCode === 13 && this.props.confirmLimit();
    }

    render() {
        return (
            <div>
                <div className="columns">
                    <div className="column is-12">
                        <Link
                            to="/limits/step-one"
                            icon="arrow-left"
                            as="button"
                            iconIsLeft
                            type="link"
                        >GO BACK</Link>
                    </div>
                </div>
                <div className="columns">
                    <div className="column is-12">
                        <p className="title">ACCOUNT LIMITS</p>
                        <article className="tile notification is-danger">
                            <p>You are about to set the following limit on your account. Please note that this limit can only be removed 7 days after you contact support requesting the removal of this limit</p>
                        </article>
                    </div>
                </div>
                <div className="columns">
                    <div className="column is-12">
                        <label className="label">Enter your password</label>
                        <Input
                            type="password"
                            onChange={(e) => this.props.TypeConfirmationPassword(e.target.value)}
                            onKeyUp={(e) => this.handleKeyUp(e)}
                            ref={node => this.pwdInput = node}
                        />
                    </div>
                </div>
                <div className="columns">
                    <div className="column is-12">
                        {this.props.fields.passwordError && (
                            <article className="tile notification is-danger">{this.props.fields.passwordError}</article>
                        )}
                        {this.props.fields.serverError && (
                            <article className="tile notification is-danger">{this.props.fields.serverError}</article>
                        )}
                    </div>
                </div>
                <div className="columns">
                    <div className="column is-12">
                        <Link
                            icon="check"
                            as="button"
                            iconIsLeft
                            type="danger"
                            onClick={() => this.props.confirmLimit()}
                        >CONFIRM LIMIT</Link>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    fields: state.formLimits,
});

const mapDispatchToProps = dispatch => ({
    confirmLimit: () => dispatch(Actions.ConfirmLimit()),
    TypeConfirmationPassword: (pwd) => dispatch(Actions.TypeConfirmationPassword(pwd)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(StepTwo);