import { createReducer } from '../../helpers/redux';
import SessionManager from '../../helpers/sessionManager';
import { history } from '../../App'

/**
 * REDUCERS
 */
const stepOneReducer = createReducer({
    bet_limit: 0,
    bet_limit_period: 0,
    deposit_limit: 0,
    loss_limit: 0,
    limitsError: null,
    password: '',
    loading: false,
    serverError: null,
    passwordError: null,
}, {
    FIELD_CHANGE: (state, action) => ({
        ...state,
        [action.payload.field]: action.payload.value,
    }),
    LIMITS_ERROR: (state, action) => ({
        ...state,
        limitsError: action.payload.message,
    }),
    TYPE_CONFIRMATION_PASSWORD: (state, action) => ({
        ...state,
        password: action.payload.pwd,
    }),
    POSTING_ACC_LIMITS_REQUEST: (state, action) => ({
        ...state,
        loading: true,
        serverError: null, 
    }),
    POSTING_ACC_LIMITS_SUCCESS: (state, action) => ({
        ...state,
        loading: false,
        serverError: null,
        password: '',
        passwordError: null,
    }),
    POSTING_ACC_LIMITS_FAILURE: (state, action) => ({
        ...state,
        loading: false,
        serverError: action.payload.message,
    }),
    PASSWORD_ERROR: (state, action) => ({
        ...state,
        passwordError: action.payload.message,
    }),
});

const stepTwoReducer = createReducer({
    password: "",
    loading: false,
});

/**
 * ACTION CREATORS
 */
const PersistantFieldChange = (field, value) => dispatch => {
    SessionManager.setItem(field, value);
    dispatch(FieldChange(field, value));
};

const FieldChange = (field, value) => ({
    type: "FIELD_CHANGE",
    payload: { field, value },
});

const TypeConfirmationPassword = (pwd) => ({
    type: "TYPE_CONFIRMATION_PASSWORD",
    payload: { pwd },
});

const SetAndValidateLimits = () => (dispatch, getState) => {
    const { formLimits } = getState();

    // Implement validation policy here
    if (formLimits.bet_limit < 0 || formLimits.deposit_limit < 0 || formLimits.loss_limit < 0) 
    {
        dispatch({ 
            type: "LIMITS_ERROR", 
            payload: { message: "All values must be positive numbers" },
        });
    } 
    else
    {
        // Reset message and redirect
        dispatch({ type: "LIMITS_ERROR", payload: { message: null }});
        history.push("/limits/step-two");
    }
}

const ConfirmLimit = () => (dispatch, getState) => {
    const state = getState();

    if (!state.formLimits.password) 
    {
        dispatch({ type: "PASSWORD_ERROR", payload: { message: "Please provide password" }});
    } 
    else if (state.formLimits.password === 'foobar') 
    {
        dispatch({ type: "POSTING_ACC_LIMITS_REQUEST", });
        postData(state.formLimits)
            .then(data => {
                dispatch({ type: "POSTING_ACC_LIMITS_SUCCESS", });
                dispatch(TypeConfirmationPassword(""));
                history.push("/limits/step-three");
            })
            .catch(error => {
                dispatch({ 
                    type: "POSTING_ACC_LIMITS_FAILURE",
                    payload: {
                        message: "We are curently not able to process your request. Please try again later.",
                    }
                });
            });
    } 
    else 
    {
        dispatch({ type: "PASSWORD_ERROR", payload: { message: "Invalid password", }});
    }
};

const postData = (state) => {
    const data = new FormData();
    data.append("json", JSON.stringify(state));

    return fetch('http://httpbin.org/post', {
        method: "POST",
        body: data,
    });
}


export { PersistantFieldChange, TypeConfirmationPassword, SetAndValidateLimits, ConfirmLimit }

export default stepOneReducer;