import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import StepOne from './stepOne';
import StepTwo from './stepTwo';
import StepThree from './stepThree';



export default class Form extends Component {
    render() {
        return (
            <div>
                <Switch>
                    <Route exact path="/limits/step-one" component={StepOne} />
                    <Route exact path="/limits/step-two" component={StepTwo} />
                    <Route exact path="/limits/step-three" component={StepThree} />
                </Switch>
            </div>
        );
    }
}