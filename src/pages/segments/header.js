import React from 'react';
import { NavLink as Link } from 'react-router-dom';

export default props => (
    <section className="hero is-dark">

        <div className="hero-body">
            <div className="container">
                <h1 className="title">
                {props.title}
                </h1>
                <h2 className="subtitle">
                {props.subtitle}
                </h2>
            </div>
        </div>
        
        <div className="hero-foot">
            <div className="container">
                <div className="columns is-centered">
                    <div className="column">
                        <Link to="/account" className="navbar-item">ACCOUNT</Link>
                    </div>
                    <div className="column">
                        <Link to="/casher" className="navbar-item">CASHER</Link>
                    </div>
                    <div className="column">
                        <Link to="/balances" className="navbar-item">BALANCES</Link>
                    </div>
                    <div className="column">
                        <Link to="/bonuses" className="navbar-item">BONUSES</Link>
                    </div>
                    <div className="column is-3">
                        <Link to="/transaction-history" className="navbar-item">TRANSACTION HISTORY</Link>
                    </div>
                    <div className="column">
                        <Link to="/limits/step-one" className="navbar-item">LIMITS</Link>
                    </div>
                </div>
            </div>
        </div>
    </section>
);