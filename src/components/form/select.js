import React, { Component } from 'react';


export default ({ value, values, onChange }) => (
    <div className="select">
        <select 
            onChange={(e) => onChange(e.target.value)}
            value={value}
        >
            {
                values.map(value => (
                    <option key={value}>{value}</option>
                ))
            }
        </select>
    </div>
)