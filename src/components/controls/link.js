import React, { Component } from 'react';
import { Link } from 'react-router-dom';


export default ({ to, as, type, icon, iconIsLeft, children, onClick }) => (
    !onClick ? (<Link
        to={to}
        className={`link is-${type} ${as}`}
        onClick={onClick}
    >
        {icon && iconIsLeft && <i className={"fa fa-" + icon} aria-hidden="true"></i>}
        {children}
        {icon && !iconIsLeft && <i className={"fa fa-" + icon} aria-hidden="true"></i>}
    </Link>) : (
        <a className={`link ${as} is-${type}`} onClick={onClick}>
            {icon && iconIsLeft && <i className={"fa fa-" + icon} aria-hidden="true"></i>}
            {children}
            {icon && !iconIsLeft && <i className={"fa fa-" + icon} aria-hidden="true"></i>}
        </a>
    )
);